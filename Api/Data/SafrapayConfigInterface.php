<?php

declare(strict_types=1);

namespace Safrapay\Magento2\Api\Data;

interface SafrapayConfigInterface
{
    public const ORDER_STATUS_NEW = 'payment/safrapay/new_order_status';
}
