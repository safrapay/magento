<?php
namespace Safrapay\Magento2\Model;

class ConfigProvider
{
    protected $scopeConfig;
    protected $assetRepo;
    protected $storeManager;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\View\Asset\Repository $assetRepo,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->assetRepo = $assetRepo;
        $this->storeManager = $storeManager;
    }

    // Common functions
    public function getStaticUrl()
    {
        return $this->assetRepo->getUrl("Safrapay_Magento2::images");
    }

    public function getTermsUrl()
    {
        $fileName = "Termos-de-Uso-Portal-Safrapay-V3-20210512.pdf";
        return $this->assetRepo->getUrl("Safrapay_Magento2::pdf/".$fileName);
    }
    public function getTermsTxt()
    {
        return "Aceito os termos e condições";
    } 
}
