# Instalação do módulo magento Safrapay


### Baixar módulo
Na raiz do projeto executar o comando:

```shell
composer require safrapay/magento2-payment
```
[link do packgist](https://packagist.org/packages/safrapay/magento2-payment)

### Iniciar a configuração do módulo na loja
```shell
bin/magento setup:upgrade
```

### Compilar o projeto/loja novamente
```shell
bin/magento setup:di:compile
```

### Configurar os campos de checkout
Na tela administrativa do magento, seguir o caminho:
```
Stores > configuration > Customers > Customer Configuration > Name and Address Options
```
E modifique o valor de "Number of Lines in a Street Address" de 2 para 4.

### Configurar o Módulo
Na tela administrativa do magento, seguir o caminho:
```
Stores > configuration > Sales > Payment Methods > others
```
Ao chegar no final da página você deve encontrar algumas opções de configuração do módulo Safrapay.

### Safrapay Pagamentos
- **Ativar** `Yes/No` 
*Ativa ou desativa módulo de pagamento Safrapay*

- **Ambiente** `Produção/Homologação`
*Seleciona entre fazer transação em produção ou em ambiente de homologação(desenvolvimento)*

- **CNPJ** `número do cnpj`
*Deve ser colocado o cnpj cadastrado na Safrapay*

- **Merchant Token** `id do merchant`
*ID do merchant da Safrapay*

- **Status do pedido criado** `Pending/Processing/suspected fraud/Complete/Closed/Canceled`
*Status inicial do pedido criado antes de receber uma confirmação de pagamento*


### Safrapay cartão de crédito

- **Ativar cartão de crédito** `Yes/No` 
*Ativa ou desativa opção de crédito*

- **Máximo de parcelas** `mínimo de 1 e máximo de 12`

- **Valor mínimo da parcela** `valor mínimo de uma parcela em R$`

###  Safrapay Boleto
- **Ativar  boleto** `Yes/No` 
*Ativa ou desativa opção de boleto*

- **Dias para vencimento do boleto** `número de dias` 
*Quantos dias para efetuar o pagamento do boleto*

- **Dias para multa** `número de dias` 
*Dias para começar aplicar multa por atraso*

- **Valor fixo da multa**  `valor em reais a ser pago na multa`

- **Valor percentual da multa**  `valor percentual a ser pago na multa`

###  Safrapay PIX
- **Ativar  Pix** `Yes/No`
  *Ativa ou desativa opção de PIX*

### Registrar Webhook

Agora para que o módulo possa se comunicar com o gateway Safrapay, iremos precisar registrar o webhook, como no exemplo abaixo:

```
POST 
{{ENDPOINT_WEBHOOK}}/v1/webhook/bulk

Body:
{
   "email": "seuemail@email.com.br",
   "webhooks": [
      {
         "targetUrl": "https://seudomínio/safrapay/apicallback",
         "eventType": 1
      },
      {
         "targetUrl": "https://seudomínio/safrapay/apicallback",
         "eventType": 2
      }
   ]
}
```

Obs: Autenticação para utilização da API será feita no endpoint {{ENDPOINT_GATEWAY}}/v2/merchant/auth