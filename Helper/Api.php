<?php

namespace Safrapay\Magento2\Helper;


use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\Order;

class Api
{
    public $enableExternalExtension = true;

    protected $customerRepository;
    protected $url;
    protected $scopeConfig;
    protected $logger;
    protected $_storeManager;
    protected $checkoutSession;

    /**
     * @var PriceCurrencyInterface
     */
    protected PriceCurrencyInterface $priceCurrency;

    /**
     * @var CartRepositoryInterface
     */
    protected CartRepositoryInterface $quoteRepository;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        CustomerRepositoryInterface $customerRepository,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Checkout\Model\Session $checkoutSession,
        PriceCurrencyInterface $priceCurrency,
        CartRepositoryInterface $quoteRepository
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->customerRepository = $customerRepository;
        $this->logger = $logger;
        $this->_storeManager = $storeManager;
        $this->checkoutSession = $checkoutSession;
        $this->priceCurrency = $priceCurrency;
        $this->quoteRepository = $quoteRepository;
    }

    /**
     * Get cc Brand by ccNumber
     * @param $cc
     * @return string
     */
    public function getCcBrand($cc)
    {
        \Safrapay\ApiSDK\Configuration::initialize();
        \Safrapay\ApiSDK\Configuration::setUrl(\Safrapay\ApiSDK\Configuration::DEV_URL);
        \Safrapay\ApiSDK\Configuration::setCnpj($this->getClientId());
        \Safrapay\ApiSDK\Configuration::setMerchantToken($this->getClientSecret());
        \Safrapay\ApiSDK\Configuration::setlog(false);
        \Safrapay\ApiSDK\Configuration::login();
        $response = \Safrapay\ApiSDK\Helper\Utils::getBrandCardBin($cc);
        if (isset($response['status']) && $response["status"] != false) {
            return $response['brand'];
        }
        return "";
    }

    /**
     * @param \Magento\Sales\Model\Order\Interceptor $order
     * @param $payment
     * @return array|null
     */
    public function createOrderBoleto(\Magento\Sales\Model\Order\Interceptor $order, $payment)
    {
        return $this->extCreateOrderBoleto($order, $payment);
    }

    /**
     * @param Order $order
     * @param $payment
     * @return array|null
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws NoSuchEntityException
     */
    public function extCreateOrderBoleto(Order $order, $payment)
    {
        \Safrapay\ApiSDK\Configuration::initialize();
        \Safrapay\ApiSDK\Configuration::setUrl($this->getApiUrl());
        \Safrapay\ApiSDK\Configuration::setCnpj($this->getClientId());
        \Safrapay\ApiSDK\Configuration::setMerchantToken($this->getClientSecret());
        \Safrapay\ApiSDK\Configuration::setlog(false);
        \Safrapay\ApiSDK\Configuration::login();

        $gateway = new \Safrapay\ApiSDK\Gateway;
        $boleto = new \Safrapay\ApiSDK\Domains\Boleto;

        $quote = $this->checkoutSession->getQuote();
        $billingAddress = $quote->getBillingAddress();
        $boleto->setSource(14);
        $boleto->setMerchantChargeId($order->getIncrementId());

        $boleto->setDeadline($this->scopeConfig->getValue(
            'payment/safrapay_boleto/expiration_days',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        ));

        $boleto->customer->setId($order->getIncrementId());

        if (empty($payment->getAdditionalInformation('boletofullname'))) {
            throw new LocalizedException(__('Preencha o Nome para concluir o pedido.'));
        }
        $boleto->customer->setName($payment->getAdditionalInformation('boletofullname'));

        if (empty($quote->getCustomerEmail())) {
            throw new LocalizedException(__('Preencha o Email para concluir o pedido.'));
        }
        $boleto->customer->setEmail($quote->getCustomerEmail());

        if (empty($payment->getAdditionalInformation('boletodocument'))) {
            throw new LocalizedException(__('Preencha o CPF/CNPJ para concluir o pedido.'));
        }
        $cpfCnpj = $payment->getAdditionalInformation('boletodocument');
        $cpfCnpj = filter_var($cpfCnpj, FILTER_SANITIZE_NUMBER_INT);

        // Insert CPF/CNPJ into customer entity for Clearsale integration
        $order->setCustomerTaxvat($cpfCnpj);
        $order->save();

        $boleto->customer->setDocumentType(\Safrapay\ApiSDK\Enum\DocumentType::CPF);
        if (strlen($cpfCnpj) == 14) {
            $boleto->customer->setDocumentType(\Safrapay\ApiSDK\Enum\DocumentType::CNPJ);
        }
        $boleto->customer->setDocument($cpfCnpj);

// Customer->address
        $street = $billingAddress->getStreet();
        if (empty($street[0])) {
            throw new LocalizedException(__('Preencha a Rua do endereço para concluir o pedido.'));
        }

        if (empty($street[1])) {
            throw new LocalizedException(__('Preencha o Número do endereço para concluir o pedido.'));
        }
        
        if (empty($street[3])) {
            throw new LocalizedException(__('Preencha o Bairro do endereço para concluir o pedido.'));
        }

        $boleto->customer->address->setStreet($street[0]);
        $boleto->customer->address->setNumber($street[1]);
        $boleto->customer->address->setComplement($street[2]);
        $boleto->customer->address->setNeighborhood($street[3]);

        if (empty($billingAddress->getCity())) {
            throw new LocalizedException(__('Preencha a Cidade do endereço para concluir o pedido.'));
        }
        $boleto->customer->address->setCity($billingAddress->getCity());

        if (empty($billingAddress->getRegion())) {
            throw new LocalizedException(__('Preencha o Estado do endereço para concluir o pedido.'));
        }
        $boleto->customer->address->setState($this->codigoUF($billingAddress->getRegion()));

        $boleto->customer->address->setCountry("BR");

        if (empty($billingAddress->getPostcode())) {
            throw new LocalizedException(__('Preencha o CEP do endereço para concluir o pedido.'));
        }
        $boleto->customer->address->setZipcode($billingAddress->getPostcode());

// Customer->phone
        if (empty($billingAddress->getTelephone())) {
            throw new LocalizedException(__('Preencha o Telefone para concluir o pedido.'));
        }
        $phone_number = filter_var($billingAddress->getTelephone(), FILTER_SANITIZE_NUMBER_INT);
        $boleto->customer->phone->setCountryCode("55");
        $boleto->customer->phone->setAreaCode(substr($phone_number, 0, 2));
        $boleto->customer->phone->setNumber(substr($phone_number, 2));
        $boleto->customer->phone->setType(\Safrapay\ApiSDK\Enum\PhoneType::MOBILE);

        foreach ($this->getItems($order) as $item) {
            $boleto->products->add(
                $item['name'],
                $item['sku'],
                $item['value'],
                $item['qty']
            );
        }

// Transactions
        $quote = $this->quoteRepository->get($order->getQuoteId());
        $grandTotal = $this->getCentsValue($quote->getGrandTotal());
        $boleto->transactions->setAmount($grandTotal);
        $boleto->transactions->setInstructions("Senhor caixa não receber após o vencimento.");

// Transactions->fine (opcional)
        if ($this->scopeConfig->getValue("payment/safrapay_boleto/fine_days")) {
            $boleto->transactions->fine
                ->setStartDate($this->scopeConfig->getValue("payment/safrapay_boleto/fine_days"));
        }
        if ($this->scopeConfig->getValue("payment/safrapay_boleto/fine_days")
            && $this->scopeConfig->getValue("payment/safrapay_boleto/fine_amount")) {
            $boleto->transactions->fine
                ->setAmount($this->scopeConfig->getValue("payment/safrapay_boleto/fine_amount"));
        }
        if ($this->scopeConfig->getValue("payment/safrapay_boleto/fine_days")
            && $this->scopeConfig->getValue("payment/safrapay_boleto/fine_percent")) {
            $boleto->transactions->fine->setInterest(10);
        }

// Transactions->discount (opcional)
//        $boleto->transactions->discount->setType(Safrapay\ApiSDK\Enum\DiscountType::FIXED);
//        $boleto->transactions->discount->setAmount(200);
//        $boleto->transactions->discount->setDeadline("1");

        $this->logger->info("External Safrapay API Request: ".json_encode($boleto));

        $result = $gateway->charge($boleto);

        $this->logger->info("External Safrapay API Return: ".json_encode($result));
        return $result;
    }

    /**
     * Get value in cents from float
     *
     * @param float $originalValue
     * @return int
     */
    public function getCentsValue(float $originalValue): int
    {
        return (int)number_format($originalValue, 2, '', '');
    }
    public function createOrderCc(Order $order, $info, $payment, $preAuth = 0)
    {
        return $this->extCreateOrderCc($order, $info, $payment, $preAuth);
    }

    public function extCreateOrderCc(Order $order, $info, $payment, $preAuth = 0)
    {
        \Safrapay\ApiSDK\Configuration::initialize();
        \Safrapay\ApiSDK\Configuration::setUrl($this->getApiUrl());
        \Safrapay\ApiSDK\Configuration::setCnpj($this->getClientId());
        \Safrapay\ApiSDK\Configuration::setMerchantToken($this->getClientSecret());
        \Safrapay\ApiSDK\Configuration::login();
        \Safrapay\ApiSDK\Configuration::setlog(false);

        $gateway = new \Safrapay\ApiSDK\Gateway;
        $authorization = new \Safrapay\ApiSDK\Domains\Authorization;
//        if($preAuth){
//            unset($authorization);
//            $authorization = new \Safrapay\ApiSDK\Domains\PreAuthorization;
//        }
        $quote = $this->checkoutSession->getQuote();
        $billingAddress = $quote->getBillingAddress();

        $this->logger->info("Card CCDC Type: ".$payment->getAdditionalInformation('cc_dc_choice'));
        if ($payment->getAdditionalInformation('cc_dc_choice')=="dc") {
            $authorization->transactions->setPaymentType(\Safrapay\ApiSDK\Enum\PaymentType::DEBIT);
        } else {
            $authorization->transactions->setPaymentType(\Safrapay\ApiSDK\Enum\PaymentType::CREDIT);
        }
        $authorization->setSource(14);
        $authorization->setMerchantChargeId($order->getIncrementId());

        if (empty($order->getBillingAddress()->getName())) {
            throw new LocalizedException(__('Preencha o Nome para concluir o pedido.'));
        }
        $authorization->customer->setName($order->getBillingAddress()->getName());

        if (empty($quote->getCustomerEmail())) {
            throw new LocalizedException(__('Preencha o Email para concluir o pedido.'));
        }
        $authorization->customer->setEmail($quote->getCustomerEmail());

        if (empty($payment->getAdditionalInformation('document'))) {
            throw new LocalizedException(__('Preencha o CPF/CNPJ para concluir o pedido.'));
        }
        $cpfCnpj = $payment->getAdditionalInformation('document');
        $cpfCnpj = filter_var($cpfCnpj, FILTER_SANITIZE_NUMBER_INT);

        // Insert CPF/CNPJ into customer entity for Clearsale integration
        $order->setCustomerTaxvat($cpfCnpj);
        $order->save();

        $authorization->customer->setDocumentType(\Safrapay\ApiSDK\Enum\DocumentType::CPF);
        if (strlen($cpfCnpj)==14) {
            $authorization->customer->setDocumentType(\Safrapay\ApiSDK\Enum\DocumentType::CNPJ);
        }
        $authorization->customer->setDocument($cpfCnpj);

        if (empty($billingAddress->getTelephone())) {
            throw new LocalizedException(__('Preencha o Telefone para concluir o pedido.'));
        }
        $authorization->customer->phone->setCountryCode("55");
        $phone_number = filter_var($billingAddress->getTelephone(), FILTER_SANITIZE_NUMBER_INT);
        $authorization->customer->phone->setAreaCode(substr($phone_number, 0, 2));
        $authorization->customer->phone->setNumber(substr($phone_number, 2));
        $authorization->customer->phone->setType(\Safrapay\ApiSDK\Enum\PhoneType::MOBILE);

        $street = $billingAddress->getStreet();
        if (empty($street[0])) {
            throw new LocalizedException(__('Preencha a Rua do endereço para concluir o pedido.'));
        }

        if (empty($street[1])) {
            throw new LocalizedException(__('Preencha o Número do endereço para concluir o pedido.'));
        }
        
        if (empty($street[3])) {
            throw new LocalizedException(__('Preencha o Bairro do endereço para concluir o pedido.'));
        }

        $authorization->customer->address->setStreet($street[0]);
        $authorization->customer->address->setNumber($street[1]);
        $authorization->customer->address->setComplement($street[2]);
        $authorization->customer->address->setNeighborhood($street[3]);

        if (empty($billingAddress->getCity())) {
            throw new LocalizedException(__('Preencha a Cidade do endereço para concluir o pedido.'));
        }
        $authorization->customer->address->setCity($billingAddress->getCity());

        if (empty($billingAddress->getRegion())) {
            throw new LocalizedException(__('Preencha o Estado do endereço para concluir o pedido.'));
        }
        $authorization->customer->address->setState($this->codigoUF($billingAddress->getRegion()));

        $authorization->customer->address->setCountry("BR");

        if (empty($billingAddress->getPostcode())) {
            throw new LocalizedException(__('Preencha o CEP do endereço para concluir o pedido.'));
        }
        $authorization->customer->address->setZipcode($billingAddress->getPostcode());

        $authorization->transactions->card
            ->setCardNumber(preg_replace('/[\-\s]+/', '', $info->getCcNumber()));
        $authorization->transactions->card->setCVV($payment->getAdditionalInformation('cc_cid'));
        $authorization->transactions->card->setCardholderName($payment->getAdditionalInformation('fullname'));
        $authorization->transactions->card->setExpirationMonth($payment->getAdditionalInformation('cc_exp_month'));
        $authorization->transactions->card->setExpirationYear($payment->getAdditionalInformation('cc_exp_year'));
        $authorization->transactions->card->setCardholderDocument($cpfCnpj);

        $authorization->transactions->card->billingAddress->setStreet($billingAddress
            ->getStreet()[0]);
        $authorization->transactions->card->billingAddress->setNumber($billingAddress
            ->getStreet()[1]);
        $authorization->transactions->card->billingAddress->setComplement($billingAddress
            ->getStreet()[2]);
        $authorization->transactions->card->billingAddress->setNeighborhood($billingAddress
            ->getStreet()[3]);
        $authorization->transactions->card->billingAddress->setCity($billingAddress->getCity());
        $authorization->transactions->card->billingAddress->setState($this->codigoUF($billingAddress->getRegion()));
        $authorization->transactions->card->billingAddress->setCountry("BR");
        $authorization->transactions->card->billingAddress->setZipcode($billingAddress->getPostcode());
        foreach ($this->getItems($order) as $item) {
            $authorization->products->add(
                $item['name'],
                $item['sku'],
                $item['value'],
                $item['qty']
            );
        }

        if ($payment->getAdditionalInformation('cc_dc_choice')!="dc") {
            $authorization->transactions->setInstallmentNumber($payment->getAdditionalInformation('installments'));
            $authorization->transactions->setInstallmentType(\Safrapay\ApiSDK\Enum\InstallmentType::NONE);
            if ($payment->getAdditionalInformation('installments')>1) {
                $authorization->transactions->setInstallmentType(\Safrapay\ApiSDK\Enum\InstallmentType::MERCHANT);
            }
        }
        $quote = $this->quoteRepository->get($order->getQuoteId());
        $grandTotal = $this->getCentsValue($quote->getGrandTotal());
        $authorization->transactions->setAmount($grandTotal);

        // $this->logger->info("External Safrapay API Request: ".$authorization->customer->document);

        $result = $gateway->charge($authorization);

        $this->logger->info("External Safrapay API Return: ".json_encode($result));
        return $result;
    }
    public function getApiUrl()
    {
        if (!$this->scopeConfig->getValue(
            'payment/safrapay/environment',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        )) {
            return \Safrapay\ApiSDK\Configuration::DEV_URL;
        }
        return \Safrapay\ApiSDK\Configuration::PROD_URL;
    }
    public function logError($action, $url, $output, $input = "")
    {
        $this->logger->error("Safrapay Request error: ".$action." - ".$url." - ".$input." - ".$output);
        return false;
    }
    public function getClientId()
    {
        return $this->scopeConfig->getValue(
            'payment/safrapay/client_id',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    public function getClientSecret()
    {
        return $this->scopeConfig->getValue(
            'payment/safrapay/client_secret',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    public function codigoUF($txt_uf)
    {
        $array_ufs = ["Rondônia" => "RO",
            "Acre" => "AC",
            "Amazonas" => "AM",
            "Roraima" => "RR",
            "Pará" => "PA",
            "Amapá" => "AP",
            "Tocantins" => "TO",
            "Maranhão" => "MA",
            "Piauí" => "PI",
            "Ceará" => "CE",
            "Rio Grande do Norte" => "RN",
            "Paraíba" => "PB",
            "Pernambuco" => "PE",
            "Alagoas" => "AL",
            "Sergipe" => "SE",
            "Bahia" => "BA",
            "Minas Gerais" => "MG",
            "Espírito Santo" => "ES",
            "Rio de Janeiro" => "RJ",
            "São Paulo" => "SP",
            "Paraná" => "PR",
            "Santa Catarina" => "SC",
            "Rio Grande do Sul (*)" => "RS",
            "Mato Grosso do Sul" => "MS",
            "Mato Grosso" => "MT",
            "Goiás" => "GO",
            "Distrito Federal" => "DF"];
        $uf = "RJ";
        foreach ($array_ufs as $key => $value) {
            if ($key == $txt_uf) {
                $uf = $value;
                break;
            }
        }
        return $uf;
    }
    public function getDocumentTypeId($type = "cpf")
    {
        if ($type=="cpf") {
            return 1;
        }
        if ($type=="cnpj") {
            return 2;
        }
        return 1;
    }
    public function getError($arrayReturn)
    {
        if (!isset($arrayReturn['charge']['transactions'][0]['errorMessage'])) {
            return "";
        }
        return $arrayReturn['charge']['transactions'][0]['errorMessage'];
    }
    public function getBoletoUrl($result)
    {
        $env = $this->scopeConfig->getValue(
            'payment/safrapay/environment',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $url = \Safrapay\ApiSDK\Configuration::DEV_URL;
        if ($env) {
            $url = \Safrapay\ApiSDK\Configuration::PROD_URL;
        }
        $url = str_replace("/v2/", "", $url);
        $bankSlipUrl = str_replace("\\", "", $result['charge']['transactions'][0]['bankSlipUrl']);
        return $url . $bankSlipUrl;
    }

    public function createOrderPix($order, $payment)
    {
        $this->logger->info('SAFRAPAY PIX create order started');
        /** @var $order \Magento\Sales\Api\Data\OrderInterface */
        \Safrapay\ApiSDK\Configuration::initialize();
        \Safrapay\ApiSDK\Configuration::setUrl($this->getApiUrl());
        \Safrapay\ApiSDK\Configuration::setCnpj($this->getClientId());
        \Safrapay\ApiSDK\Configuration::setMerchantToken($this->getClientSecret());
        \Safrapay\ApiSDK\Configuration::setlog(false);
        \Safrapay\ApiSDK\Configuration::login();
        $gateway = new \Safrapay\ApiSDK\Gateway;
        $pix = new \Safrapay\ApiSDK\Domains\Pix;
        
        $pix->setSource(14);
        $pix->setMerchantChargeId($order->getIncrementId());
        
// Customer
        $pix->customer->setId($order->getIncrementId());

        if (empty($payment->getAdditionalInformation('pixfullname'))) {
            throw new LocalizedException(__('Preencha o Nome para concluir o pedido.'));
        }
        $pix->customer->setName($payment->getAdditionalInformation('pixfullname'));

        if (empty($order->getCustomerEmail())) {
            throw new LocalizedException(__('Preencha o Email para concluir o pedido.'));
        }
        $pix->customer->setEmail($order->getCustomerEmail());

        if (empty($payment->getAdditionalInformation('pixdocument'))) {
            throw new LocalizedException(__('Preencha o CPF/CNPJ para concluir o pedido.'));
        }
        $cpfCnpj = $payment->getAdditionalInformation('pixdocument');
        $cpfCnpj = filter_var($cpfCnpj, FILTER_SANITIZE_NUMBER_INT);

        // Insert CPF/CNPJ into customer entity for Clearsale integration
        $order->setVatId($cpfCnpj);
        $order->save();

        $pix->customer->setDocumentType(\Safrapay\ApiSDK\Enum\DocumentType::CPF);
        if (strlen($cpfCnpj) == 14) {
            $pix->customer->setDocumentType(\Safrapay\ApiSDK\Enum\DocumentType::CNPJ);
        }

        $pix->customer->setDocument($cpfCnpj);
// Customer->address
        $quote = $this->checkoutSession->getQuote();
        $billingAddress = $quote->getBillingAddress();


        $street = $billingAddress->getStreet();
        if (empty($street[0])) {
            throw new LocalizedException(__('Preencha a Rua do endereço para concluir o pedido.'));
        }

        if (empty($street[1])) {
            throw new LocalizedException(__('Preencha o Número do endereço para concluir o pedido.'));
        }
        
        if (empty($street[3])) {
            throw new LocalizedException(__('Preencha o Bairro do endereço para concluir o pedido.'));
        }

        $pix->customer->address->setStreet($street[0]);
        $pix->customer->address->setNumber($street[1]);
        $pix->customer->address->setComplement($street[2]);
        $pix->customer->address->setNeighborhood($street[3]);

        if (empty($billingAddress->getCity())) {
            throw new LocalizedException(__('Preencha a Cidade do endereço para concluir o pedido.'));
        }
        $pix->customer->address->setCity($billingAddress->getCity());

        if (empty($billingAddress->getRegion())) {
            throw new LocalizedException(__('Preencha o Estado do endereço para concluir o pedido.'));
        }
        $pix->customer->address->setState($this->codigoUF($billingAddress->getRegion()));

        $pix->customer->address->setCountry("BR");

        if (empty($billingAddress->getPostcode())) {
            throw new LocalizedException(__('Preencha o CEP do endereço para concluir o pedido.'));
        }
        $pix->customer->address->setZipcode($billingAddress->getPostcode());

// Customer->phone
        $phone_number = filter_var($billingAddress->getTelephone(), FILTER_SANITIZE_NUMBER_INT);
        $pix->customer->phone->setCountryCode("55");
        $pix->customer->phone->setAreaCode(substr($phone_number, 0, 2));
        $pix->customer->phone->setNumber(substr($phone_number, 2));
        $pix->customer->phone->setType(\Safrapay\ApiSDK\Enum\PhoneType::MOBILE);

        foreach ($this->getItems($order) as $item) {
            $pix->products->add(
                $item['name'],
                $item['sku'],
                $item['value'],
                $item['qty']
            );
        }
// Transactions
        $quote = $this->quoteRepository->get($order->getQuoteId());
        $grandTotal = $this->getCentsValue($quote->getGrandTotal());
        $pix->transactions->setAmount($grandTotal);
        $result = $gateway->charge($pix);
        $this->logger->info(json_encode($result));
        return $result;
    }

    /**
     * Get items from best possibility
     *
     * @param OrderInterface $order
     * @return array
     * @throws NoSuchEntityException
     */
    public function getItems(OrderInterface $order): array
    {
        if (!$items = $this->getItemsDiscountAlreadyApplied($order)) {
            if (!$items = $this->getItemsDiscountNotAppliedDivideDiscount($order)) {
                if (!$items = $this->getItemsAndApplyDiscount($order)) {
                    if (!$items = $this->getItemsNormalizedShippingAlreadyAppliedDiscount($order)) {
                        $items = $this->getGeneralNormalizedItems($order);
                    }
                }
            }
        }
        return $items;
    }

    /**
     * Get items if we can trust that discount has already been applied
     *
     * @param OrderInterface $order
     * @return array|null
     */
    public function getItemsDiscountAlreadyApplied(OrderInterface $order): ?array
    {
        $grandTotal = 0;
        foreach ($order->getItems() as $item) {
            $items[] = [
                'name' => $item->getName(),
                'sku' => $item->getSku(),
                'value' => $this->getCentsValue($item->getPrice()),
                'qty' => $item->getQtyOrdered()
            ];
            $grandTotal += $this->getCentsValue($item->getPrice()) * $item->getQtyOrdered();
        }
        if ($order->getShippingAmount() > 0.0) {
            $items[] = [
                'name' => 'Envio',
                'sku' => 'Envio',
                'value' => $this->getCentsValue($order->getShippingAmount()),
                'qty' => 1
            ];
        }
        $grandTotal += $this->getCentsValue($order->getShippingAmount());
        if ($this->getCentsValue($order->getGrandTotal()) == $grandTotal) {
            return $items;
        }
        return null;
    }

    /**
     * Get items, apply discounts divided by item qty
     *
     * @param OrderInterface $order
     * @return array|null
     */
    public function getItemsDiscountNotAppliedDivideDiscount(OrderInterface $order): ?array
    {
        $grandTotal = 0;
        foreach ($order->getItems() as $item) {
            $items[] = [
                'name' => $item->getName(),
                'sku' => $item->getSku(),
                'value' => $this->getCentsValue($item->getPrice() * 0.01 * (100 - $item->getDiscountPercent())),
                'qty' => $item->getQtyOrdered()
            ];
            $grandTotal += $this->getCentsValue(
                $item->getPrice() * 0.01 * (100 - $item->getDiscountPercent())
                ) * $item->getQtyOrdered();
        }
        if ($order->getShippingAmount() - $order->getShippingDiscountAmount() > 0.0) {
            $items[] = [
                'name' => 'Envio',
                'sku' => 'Envio',
                'value' => $this->getCentsValue($order->getShippingAmount() - $order->getShippingDiscountAmount()),
                'qty' => 1
            ];
            $grandTotal += $this->getCentsValue($order->getShippingAmount() - $order->getShippingDiscountAmount());
        }

        if ($this->getCentsValue($order->getGrandTotal()) == $grandTotal) {
            return $items;
        }
        return null;
    }

    /**
     * Get items and apply discount if subtotal and shipping values sum are OK
     *
     * @param OrderInterface $order
     * @return array|null
     */
    public function getItemsAndApplyDiscount(OrderInterface $order): ?array
    {
        $grandTotal = 0;
        foreach ($order->getItems() as $item) {
            $items[] = [
                'name' => $item->getName(),
                'sku' => $item->getSku(),
                'value' => $this->getCentsValue($item->getPrice() - $item->getDiscountAmount()),
                'qty' => $item->getQtyOrdered()
            ];
            $grandTotal += $this->getCentsValue($item->getPrice() - $item->getDiscountAmount())
                * $item->getQtyOrdered();
        }
        if ($order->getShippingAmount() - $order->getShippingDiscountAmount() > 0.0) {
            $items[] = [
                'name' => 'Envio',
                'sku' => 'Envio',
                'value' => $this->getCentsValue($order->getShippingAmount() - $order->getShippingDiscountAmount()),
                'qty' => 1
            ];
            $grandTotal += $this->getCentsValue($order->getShippingAmount());
        }
        if ($this->getCentsValue($order->getGrandTotal()) == $grandTotal) {
            return $items;
        }
        return null;
    }

    /**
     * Try values without discount, if not apply discount and forcefully add shipping
     *
     * @param OrderInterface $order
     * @return array|null
     */
    public function getItemsNormalizedShippingAlreadyAppliedDiscount(OrderInterface $order): ?array
    {
        $subTotal = 0;
        $items = [];
        foreach ($order->getItems() as $item) {
            $items[] = [
                'name' => $item->getName(),
                'sku' => $item->getSku(),
                'value' => $this->getCentsValue($item->getPrice()),
                'qty' => $item->getQtyOrdered()
            ];
            $subTotal += $this->getCentsValue($item->getPrice()) * $item->getQtyOrdered();
        }

        if ($subTotal > $this->getCentsValue($order->getGrandTotal())) {
            $items = [];
            $subTotal = 0;
            foreach ($order->getItems() as $item) {
                $items[] = [
                    'name' => $item->getName(),
                    'sku' => $item->getSku(),
                    'value' => $this->getCentsValue($item->getPrice() - $item->getDiscountAmount()),
                    'qty' => $item->getQtyOrdered()
                ];
                $subTotal += $this->getCentsValue($item->getPrice() - $item->getDiscountAmount())
                    * $item->getQtyOrdered();
            }
            if ($subTotal > $this->getCentsValue($order->getGrandTotal())) {
                return null;
            }
        }
        if ($subTotal < $this->getCentsValue($order->getGrandTotal())) {
            $items[] = [
                'name' => 'Envio',
                'sku' => 'Envio',
                'value' => $this->getCentsValue($order->getGrandTotal()) - $subTotal,
                'qty' => 1
            ];
        }
        return $items;
    }

    /**
     * Divide grand total by total items and adds shipping if there is a difference
     *
     * @param OrderInterface $order
     * @return array
     * @throws NoSuchEntityException
     */
    public function getGeneralNormalizedItems(OrderInterface $order): array
    {
        $quote = $this->quoteRepository->get($order->getQuoteId());
        $grandTotal = $this->getCentsValue($quote->getGrandTotal());
        $totalItemsQty = 0;
        foreach ($order->getItems() as $item) {
            $totalItemsQty += $item->getQtyOrdered();
        }
        $unitValue = (int)floor($grandTotal / $totalItemsQty);
        $items = [];
        foreach ($order->getItems() as $item) {
            $items[] = [
                'name' => $item->getName(),
                'sku' => $item->getSku(),
                'value' => $unitValue,
                'qty' => $item->getQtyOrdered()
            ];
        }
        if ($grandTotal > $totalItemsQty * $unitValue) {
            $items[] = [
                'name' => 'Envio',
                'sku' => 'Envio',
                'value' => (int)($grandTotal - $totalItemsQty * $unitValue),
                'qty' => 1
            ];
        }
        return $items;
    }

    public function cancelOrder($safrapay_id) {
        $this->logger->info('SAFRAPAY cancel order started');
        \Safrapay\ApiSDK\Configuration::initialize();
        \Safrapay\ApiSDK\Configuration::setUrl($this->getApiUrl());
        \Safrapay\ApiSDK\Configuration::setCnpj($this->getClientId());
        \Safrapay\ApiSDK\Configuration::setMerchantToken($this->getClientSecret());
        \Safrapay\ApiSDK\Configuration::setlog(false);
        \Safrapay\ApiSDK\Configuration::login();
        $gateway = new \Safrapay\ApiSDK\Gateway;
        
        $result = $gateway->cancel($safrapay_id);
        $this->logger->info(json_encode($result));
        return $result;
    }
}
