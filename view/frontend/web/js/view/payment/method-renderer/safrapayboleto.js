/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*browser:true*/
/*global define*/
define(
    [
        'Magento_Checkout/js/view/payment/default'
    ],
    function (Component) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Safrapay_Magento2/payment/boleto'
            },
            initObservable: function () {
                this._super()
                    .observe([
                        'boletofullname',
                        'boletodocument'
                    ]);

                return this;
            },
            getData: function () {
                return {
                    'method': this.item.method,
                    'additional_data': {
                        'boletofullname': jQuery('#'+this.getCode() + '_boletofullname').val(),
                        'boletodocument': jQuery('#'+this.getCode() + '_boletodocument').val()
                    }
                };
            },
            getInstruction: function() {
                return window.checkoutConfig.payment.safrapayboleto.instruction;
            },
            getDue: function() {
                return window.checkoutConfig.payment.safrapayboleto.due;
            },
            getFullName: function() {
                return window.checkoutConfig.payment.safrapayboleto.fullname;
            },
            getTaxVat: function() {
                return window.checkoutConfig.payment.safrapayboleto.taxvat;
            },
            getTermsHtml: function () {
                return '<a target="_blank" href="' + this.getTermsUrl() +
                    '">' + this.getTermsTxt() + '</a>';
            },
            getTermsUrl: function () {
                return window.checkoutConfig.payment.safrapaycc.terms_url;
            },
            getTermsTxt: function () {
                return window.checkoutConfig.payment.safrapaycc.terms_txt;
            },
        });
    }
);
