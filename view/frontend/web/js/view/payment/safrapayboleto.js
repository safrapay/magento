define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'safrapayboleto',
                component: 'Safrapay_Magento2/js/view/payment/method-renderer/safrapayboleto'
            }
        );
        return Component.extend({});
    }
);
