define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'safrapaycc',
                component: 'Safrapay_Magento2/js/view/payment/method-renderer/safrapaycc'
            }
        );
        return Component.extend({});
    }
);
