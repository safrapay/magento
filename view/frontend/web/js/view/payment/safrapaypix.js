define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'safrapaypix',
                component: 'Safrapay_Magento2/js/view/payment/method-renderer/safrapaypix'
            }
        );
        return Component.extend({});
    }
);
