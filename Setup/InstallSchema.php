<?php

namespace Safrapay\Magento2\Setup;

class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{
    public function install(
        \Magento\Framework\Setup\SchemaSetupInterface $setup,
        \Magento\Framework\Setup\ModuleContextInterface $context
    ) {
        $setup->startSetup();

        $table = $setup->getConnection()
            ->newTable($setup->getTable('safrapay_config'))
            ->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'safrapay_option',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false, 'default' => ''],
                'Option name'
            )
            ->addColumn(
                'safrapay_value',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                65535,
                ['nullable' => false, 'default' => ''],
                'Value'
            )
            ->setComment("Safrapay Config");
        $setup->getConnection()->createTable($table);

        $sql = "INSERT INTO safrapay_config (safrapay_option,safrapay_value) VALUES ('token_value',' ')";
        $setup->getConnection()->query($sql);
        $sql = "INSERT INTO safrapay_config (safrapay_option,safrapay_value) VALUES ('token_expires','0')";
        $setup->getConnection()->query($sql);

        $table = $setup->getConnection()
            ->newTable($setup->getTable('safrapay_order'))
            ->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'increment_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => false, 'unsigned' => true, 'nullable' => false, 'primary' => false],
                'Increment ID'
            )
            ->addColumn(
                'safrapay_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false, 'default' => ''],
                'Safrapay ID'
            )
            ->addColumn(
                'amount',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => false, 'unsigned' => true, 'nullable' => false, 'primary' => false],
                'Amount'
            )
            ->addColumn(
                'status',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false, 'default' => ''],
                'Status'
            )
            ->setComment("Safrapay Orders");
        $setup->getConnection()->createTable($table);

        $setup->endSetup();
    }
}
