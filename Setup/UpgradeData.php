<?php

namespace Safrapay\Magento2\Setup;

use Magento\Customer\Model\Customer;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

class UpgradeData implements UpgradeDataInterface
{
    private $eavSetupFactory;

    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            //Add CPF attribute to the customer entity for Clearsale integration
            $eavSetup->addAttribute(
                Customer::ENTITY,
                'safrapay_cpf',
                [
                    'type' => 'varchar',
                    'label' => 'CPF',
                    'input' => 'text',
                    'required' => false,
                    'visible' => true,
                    'user_defined' => true,
                    'position' => 999,
                    'system' => 0,
                ]
            );

            $attribute = $eavSetup->getEavConfig()->getAttribute(Customer::ENTITY, 'safrapay_cpf');
            $attribute->setData(
                'used_in_forms',
                ['adminhtml_customer', 'customer_account_create', 'customer_account_edit']
            );
            $attribute->save();
        }

        $setup->endSetup();
    }
}