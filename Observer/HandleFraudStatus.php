<?php

namespace Safrapay\Magento2\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Sales\Model\Order;
use Magento\Payment\Model\Method\Factory;
use Psr\Log\LoggerInterface;

class HandleFraudStatus implements ObserverInterface
{
    protected $logger;
    protected $methodFactory;

    public function __construct(
        LoggerInterface $logger,
        Factory $methodFactory
    ) {
        $this->logger = $logger;
        $this->methodFactory = $methodFactory;
    }

    public function execute(Observer $observer)
    {
        /** @var Order $order */
        $order = $observer->getEvent()->getOrder();
        
        // Check if new status is fraud
        if ($order->getStatus() === Order::STATUS_FRAUD) {
            try {
                $payment = $order->getPayment();
                
                // Check if it's a Safrapay credit card payment
                if ($payment->getMethod() === 'safrapaycc') {
                    $amount = $order->getGrandTotal();
                    $payment->getMethodInstance()->refund($payment, $amount);
                    
                    $this->logger->info('Safrapay automatic fraud refund triggered for order ' . $order->getIncrementId());
                }
            } catch (\Exception $e) {
                $this->logger->error('Error processing fraud status refund: ' . $e->getMessage());
            }
        }
    }
}